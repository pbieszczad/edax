<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="css/normalize.css" />
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700|Roboto+Condensed:300,400,700&amp;subset=latin-ext" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.2/css/swiper.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" media="screen" href="css/hamburgers.min.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="css/main.css" />
    

</head>
<body>
<?php require_once('php/header.php');  ?>
<div class="main-content">
  <div class="container">
    <div class="slider-block">
      <h1>EDAX</h1>
      <div class="swiper-container">
        <div class="swiper-wrapper">
            <div class="swiper-slide">Międzynarodowe Biuro Podatkowe <br>Lider w odzyskiwaniu podatków z zagranicy. </div>
            <div class="swiper-slide">Międzynarodowe Biuro Podatkowe <br>Lider w odzyskiwaniu podatków z zagranicy2.</div>
            <div class="swiper-slide">Międzynarodowe Biuro Podatkowe <br>Lider w odzyskiwaniu podatków z zagranicy3.</div>
        </div>

        
          <div class="swiper-pagination"></div>
          <div class="swip-navi">
            <div class="counter">
              <span>01</span>
              <span>/</span>
              <span>01</span>
            </div>
            <div class="nav">
              <div class="swiper-button-prev"></div>
              <div class="swiper-button-next"></div>
            </div>
            
          </div>
       
        
      </div>
    </div>
    
  </div>
  
</div>
<div class="aboutUs sector">
	<img src="img/bg1.png" class="bg1" alt="">
	<img src="img/bg2.png" class="bg2" alt="">
    <div class="container">
      <h2><span>O nas</span></h2>
      <div class="text">
      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. <br><br>

      Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit.<br><br>

      Sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.<br>
      </div>
    </div>
  </div>
  <div class="prizeList sector">
		<img src="img/money.png" class="moneyBg" alt="">
    <div class="container">
      <h2><span>Cennik</span></h2>
      <div class="withIco">Cena za rozliczenie ustalana jest indywidualnie, gdyż stali klienci mogą liczyc na rabaty.</div>
      <div class="greenInfo">PEŁNY CENNIK DOSTĘPNY W BIURZE.</div>
    </div>
  </div>
  <div class="calculatorWrap">
    <div class="container">
      <div class="calc">
        <div class="title">Przelicznik <span>waluty</span></div>
        <div class="calcBlock">
          <div class="prizeInput">
            <input type="text" placeholder="Wpisz kwotę">
            <select class="select2">
              <option>PLN</option>
              <option>EURO</option>
              <option>GPB</option>
            </select>
          </div>
          <div class="calcEqal">
              <label>PRZELICZ NA</label>
              <select class="select2">
                <option>PLN</option>
                <option>EURO</option>
                <option>GPB</option>
              </select>
              <button type="button">PRZELICZ  <span> WALUTĘ</span></button>
          </div>
          <div class="equalBlock">
            <span class="label">KWOTA:</span>
            <span class="current">245 GBP </span>
            <span class="light"> / 1100 PLN</span>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="faq sector">
		<img src="img/bg4.png" class="bg4" alt="">
    <div class="container">
      <h2><span>FAQ</span></h2>
      <div class="title">Poniżej mogą państwo zapoznać się z najczęściej zadawanymi pytaniami.</div>
      <div class="faqBlock">
        <div class="item">
          <div class="itemTitleWrap">
            <div class="itemTitle">Integer eu enim a ante tristique tristique. Donec eget urna ultricies, mattis tellus ac?</div>
            <div class="itemArrow"></div>
          </div>
          <div class="itemContent">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
          </div>
        </div>
        <div class="item">
          <div class="itemTitleWrap">
            <div class="itemTitle">Integer eu enim a ante tristique tristique. Donec eget urna ultricies, mattis tellus ac?</div>
            <div class="itemArrow"></div>
          </div>
          <div class="itemContent">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
          </div>
        </div>
        <div class="item">
          <div class="itemTitleWrap">
            <div class="itemTitle">Integer eu enim a ante tristique tristique. Donec eget urna ultricies, mattis tellus ac?</div>
            <div class="itemArrow"></div>
          </div>
          <div class="itemContent">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
          </div>
        </div>
        <div class="item">
          <div class="itemTitleWrap">
            <div class="itemTitle">Integer eu enim a ante tristique tristique. Donec eget urna ultricies, mattis tellus ac?</div>
            <div class="itemArrow"></div>
          </div>
          <div class="itemContent">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
          </div>
        </div>
        <div class="item">
          <div class="itemTitleWrap">
            <div class="itemTitle">Integer eu enim a ante tristique tristique. Donec eget urna ultricies, mattis tellus ac?</div>
            <div class="itemArrow"></div>
          </div>
          <div class="itemContent">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
          </div>
        </div>
      </div>
    </div>
  </div>
<div class="contact sector">
	<img src="img/map-desktop.png" class="map-desktop" alt="">
	<img src="img/bg3.png" class="bg3" alt="">
  <div class="container">
    <h2><span>Kontakt</span></h2>
    <div class="contactContent">
      <div class="adresWrap">
        <div class="adresBlock">
          <div class="adressTitle">Rzeszów</div>
          <div class="adressContent">
            ul. Rejtana 67<br>
            35-959 Rzeszów<br><br>

            tel/fax. 17 863 30 62<br>
            kom. 790 333 424
          </div>
        </div>
        <div class="adresBlock">
          <div class="adressTitle">Lublin</div>
          <div class="adressContent">
            pl. Zamkowy 6A/6<br>
            20-121 Lublin<br><br>

            tel/fax. 81 532 16 11<br>
            kom. 504 202 180
          </div>
        </div>
      </div>
      <div class="contactForm">
				<div class="formTitle">Masz pytania?</div>
				<div class="formSubtitle">Skorzystaj z formualrza kontaktowego</div>
				<div class="formWrap">
					<div class="form-group">
						<div class="label">Imię i Nazwisko / Firma</div>
						<input type="text" >
					</div>
					<div class="form-group">
						<div class="label">Adres email</div>
						<input type="text" >
					</div>
				</div>
				<div class="textarea-group">
					<div class="label">Adres email</div>
					<textarea></textarea>
				</div>
				<div class="submitWrap">
					<button type="submit">WYŚLIJ WIADOMOŚĆ</button>
				</div>
			</div>
    </div>
  </div>
</div>
<div class="footer">
	<img src="img/bg5.png" class="bg5" alt="">
	<div class="container">
		<div class="footerWrap">
			<div class="left-side">
				<div class="footer-menu">                                 
					<div class="item"><a href="">STRONA GŁÓWNA</a></div>
					<div class="item"><a href="">O NAS</a></div>
					<div class="item"><a href="">NIEMCY</a></div>
					<div class="item"><a href="">AUSTRIA</a></div>
					<div class="item"><a href="">HOLANDIA</a></div>
					<div class="item"><a href="">ANGLIA</a></div>
					<div class="item"><a href="">IRLANDIA</a></div>
					<div class="item"><a href="">FAQ</a></div>
					<div class="item"><a href="">KONTAKT</a></div>
				</div>
				<div class="copyright">Wszelkie prawa zastrzeżone EDAX 2018</div>
			</div>
			<div class="right-side">
					Design:<span>Jednorożek Studio</span>Coded:<span>x11</span>
			</div>
		</div>
	</div>
	
</div>
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.2/js/swiper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="js/main.js"></script>
</body>
</html>