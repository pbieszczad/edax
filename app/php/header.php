<div class="header">
    <div class="header-wrap">
        <div class="logo">
            <a href="/">
                <img src="../img/logo.png" alt="">
            </a>
        </div>
        <div class="main-menu-block">
            <div class="hamburger hamburger--elastic js-hamburger">
                <div class="hamburger-box">
                    <div class="hamburger-inner"></div>
                </div>
            </div>
            <div class="main-menu">
                <div class="contact-block">
                    <div class="email">
                        <a href="mailto:ebiuro@icn.pl">ebiuro@icn.pl</a>
                    </div>
                    <div class="telephone">
                        <a class="first" href="tel:17 863 30 62">17 863 30 62</a>
                        <span>/</span>
                        <a href="tel:790 333 424">790 333 424</a>
                    </div>
                </div>
                <div class="menu">
                    <div class="item"><a href="">STRONA GŁÓWNA</a></div>
                    <div class="item"><a href="">O NAS</a></div>
                    <div class="item"><a href="">NIEMCY</a></div>
                    <div class="item"><a href="">AUSTRIA</a></div>
                    <div class="item"><a href="">HOLANDIA</a></div>
                    <div class="item"><a href="">ANGLIA</a></div>
                    <div class="item"><a href="">IRLANDIA</a></div>
                    <div class="item"><a href="">FAQ</a></div>
                    <div class="item"><a href="">KONTAKT</a></div>
                </div>
            </div>
        </div>
    </div>
</div>