$(document).ready(function(){
    console.log('jquery loaded')
    var mySwiper = new Swiper('.swiper-container', {
        speed: 400,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        pagination: {
            el:".swiper-pagination",
            type:"progressbar"
        },
    });

    $('.select2').select2({
        minimumResultsForSearch: Infinity
    });


    $('.faqBlock .item .itemTitleWrap').click(function(){
        $(this).parent().toggleClass('active')
    })

    $('.js-hamburger').click(function(){
        $(this).toggleClass('is-active')
        $('.main-menu').toggleClass('active')
    })
})